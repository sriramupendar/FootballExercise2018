﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FootballExercise
{
    public class Helper
    {
        public static List<string> GetClassProperties(Type objType)
        {
            List<string> lstProperties = new List<string>();

            if (objType != null && objType.IsClass)
            {
                PropertyInfo[] propertyInfos = objType.GetProperties();

                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    lstProperties.Add(propertyInfo.Name);
                }
            }
            return lstProperties;
        }
    }
}
