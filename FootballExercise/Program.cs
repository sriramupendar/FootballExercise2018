﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            Type objType = typeof(TeamStats);

            List<string> properties = Helper.GetClassProperties(objType);

            DataProcessor objDP = new DataProcessor();

            bool isValidStructure = objDP.ValidateStructure(properties);           
            if (isValidStructure)
            {
                try
                {
                    int diff = 0;
                    string team = string.Empty;
                    Dictionary<string, int> dict = new Dictionary<string, int>();
                    List<TeamStats> lstTeamStats = objDP.GetTeamStats();

                    if (lstTeamStats.Count > 0)
                    {
                        foreach (var item in lstTeamStats)
                        {
                            if (item.F > item.A)
                            {
                                diff = item.F - item.A;
                            }
                            else
                            {
                                diff = item.A - item.F;
                            }
                            team = item.Team;
                            dict.Add(team, diff);
                        }
                        var teamName = dict.OrderBy(k => k.Value).FirstOrDefault().Key;
                        var diffValue = dict.OrderBy(k => k.Value).FirstOrDefault().Value;                       
                        Console.WriteLine("Team : " + teamName + " with the smallest difference between for and against goals is :" + diffValue);
                        Console.ReadLine();
                    }


                    else
                    {
                        Console.WriteLine("No records found in the data file to process...please fill data and retry...");
                        Console.ReadLine();
                    }
                }
                catch
                {
                    Console.WriteLine("Failed in Diffrence Calculation in Program.cs");                    
                }

            }
            else
            {
                Console.WriteLine("Data file is not in required structure...");
                Console.ReadLine();
            }
        }

    }
}
