﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballExercise
{
    class TeamStats
    {
        [DisplayName("Team Name")]
        public string Team { get; set; }

        [DisplayName("Matches Played")]
        public int P { get; set; }

        [DisplayName("Matches Won")]
        public int W { get; set; }

        [DisplayName("Matches Drawn")]
        public int D { get; set; }

        [DisplayName("Matches Lost")]
        public int L { get; set; }

        [DisplayName("Goals Scored")]
        public int F { get; set; }

        [DisplayName("Goals Conceded")]
        public int A { get; set; }

        [DisplayName("Points")]
        public int Pts { get; set; }
    }
}
