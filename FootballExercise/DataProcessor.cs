﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballExercise
{
    class DataProcessor
    {
        string connString = string.Empty;
        string dataFilePath = string.Empty;
        public DataProcessor()
        {
            dataFilePath = System.Configuration.ConfigurationManager.AppSettings["DataFilePath"];
            connString = string.Format(
                @"Provider=Microsoft.Jet.OleDb.4.0; Data Source={0};Extended Properties=""Text;HDR=YES;FMT=Delimited""",
                Path.GetDirectoryName(dataFilePath)
            );
        }
        /// <summary>
        /// Validates the Column Structure of input data file
        /// </summary>
        /// <param name="properties">List of properties of related entity</param>
        /// <returns>Indicates true or false</returns>
        
        public bool ValidateStructure(List<string> properties)
        {
            bool isValid = true;

            using (var conn = new OleDbConnection(connString))
            {
               
                try
                {
                    conn.Open();
                    using (var cmd = new OleDbCommand("select * from [" + Path.GetFileName(dataFilePath) + "]", conn))
                    using (var reader = cmd.ExecuteReader(CommandBehavior.SchemaOnly))
                    {
                        var schema = reader.GetSchemaTable();

                        List<string> columnNames = schema.AsEnumerable().Select(r => r.Field<String>("ColumnName")).ToList();

                        foreach (string prop in properties)
                        {
                            if (!columnNames.Contains(prop))
                            {
                                isValid = false;
                                break;
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(" Invalid File or Path \r \n" + e.Message);
                    Console.ReadLine();
                }
            }

            return isValid;
        }
        /// <summary>
        /// Load the data from CSV file
        /// </summary>
        /// <returns>List of team Statstics records</returns>
        
        public List<TeamStats> GetTeamStats()
        {
            List<TeamStats> lstStats = new List<TeamStats>();

            using (var conn = new OleDbConnection(connString))
            {
                conn.Open();

                var query = "SELECT * FROM [" + Path.GetFileName(dataFilePath) + "]";
                using (var adapter = new OleDbDataAdapter(query, conn))
                {
                    var ds = new DataSet("CSV File");
                    adapter.Fill(ds);
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        try
                        {
                            lstStats.Add(
                                            new TeamStats
                                            {
                                                Team = row["Team"].ToString(),
                                                P = Convert.ToInt32(row["P"]),
                                                W = Convert.ToInt32(row["W"]),
                                                L = Convert.ToInt32(row["L"]),
                                                D = Convert.ToInt32(row["D"]),
                                                F = Convert.ToInt32(row["F"]),
                                                A = Convert.ToInt32(row["A"]),
                                                Pts = Convert.ToInt32(row["Pts"])
                                            }
                                            );
                        }
                        catch 
                        {                                                      
                            //ignoring in valid data in any row                           
                        }
                    }
                }
            }

            return lstStats;
        }
    }
}
